# Déclaration des bibliothèques utilisées
import log
import math
import machine

from time import sleep_ms

from microbit import i2c, uart, reset
from microbit import pin0, pin14, pin15 # Adapter en fonction du cablage

from mb_i2c_utils import scan_i2c_sensors
from mb_bmp390 import BMP3XX_I2C
from mb_apc220 import Apc220, uart_reset

# Initialisation de l'enregistrement dans un fichier
# log.delete()
...


# Initialisation des capteurs
i2c.init()
addresses = scan_i2c_sensors()
if len(addresses) > 4:
    machine.reset()

   
# Initialisation capteur pression
...

# Initialisation radio
...

# Boucle sans fin
while ... :
    
    # Lecture pression
    # Lecture thermisCalcule température
    pression = ...
       
    # Lecture analogique pour le calcule de la thermistance
    U0 = ...
    
    # Calcul de la température 
    temperature = ...
    
    # affichage
    ...

    # Enregistrement sur la carte
    ...
    
    # Transmission radio
    ... 
   
    # On souffle
    sleep_ms(500)
    

