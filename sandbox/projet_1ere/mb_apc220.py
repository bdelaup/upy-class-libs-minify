from microbit import *
import time

class Apc220:
    def __init__(self, tx, rx):
        self.tx = tx
        self.rx = rx
    
    def send(self, *args):
        uart.init(9600, tx=self.tx , rx=self.rx)
        print(*args)
        uart.init(115200)
        
    def receive(self, timeout=1000):
        uart.init(9600, tx=self.tx , rx=self.rx)
        start = time.ticks_ms()
        ret = None
        while time.ticks_diff(time.ticks_ms(), start) < timeout and not uart.any():
            sleep(10)
        if uart.any():
            sleep(10)
            ret = uart.readline()
        uart.init(115200)
        return ret

def uart_reset():
    uart.init(115200)
    print("=== UART - reset window open ===")
    sleep(1000)
    print("=== UART - reset window close ===")
        
if __name__ == "__main__":
    uart_reset()
    apc = Apc220(tx=pin14, rx=pin15)
    
    while (True):
        print ("=> Tic ")
        apc.send("hello",123)
        
        val = apc.receive(1000)
        if val != None:
            print ("<= ", val)
    

