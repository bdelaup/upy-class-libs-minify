"""
MicroPython driver for SI1145 light I2C sensor, developer version, specific for BBC Micro:bit :
https://github.com/neliogodoi/MicroPython-SI1145
Version: 0.2.0 @ 2018/06/14
"""
from time import sleep

class SI1145:
    """Driver for SI1145 light sensor with I2C interface"""
    
    def __init__(self, i2c, addr=0x60):
        self._i2c = i2c
        self._addr = addr
        self._reset()
        self._load_calibration()

    def _reset(self):
        """Reset the hardware sensor"""
        
        self._write8(0x08, 0x00)
        self._write8(0x09, 0x00)
        self._write8(0x04, 0x00)
        self._write8(0x05, 0x00)
        self._write8(0x06, 0x00)
        self._write8(0x03, 0x00)
        self._write8(0x21, 0xFF)
        self._write8(0x18, 0x01)
        sleep(.01)
        self._write8(0x07, 0x17)
        sleep(.01)

    def _load_calibration(self):
        """Settings of calibration of the sensor"""
        
        self._write8(0x13, 0x7B)
        self._write8(0x14, 0x6B)
        self._write8(0x15, 0x01)
        self._write8(0x16, 0x00)
        self._write_param(0x01, 0x80 | 0x40 | 0x20 | 0x10 | 0x01)
        self._write8(0x03, 0x01)
        self._write8(0x04, 0x01)
        self._write8(0x0F, 0x03)
        self._write_param(0x07, 0x03)
        self._write_param(0x02, 0x01)
        self._write_param(0x0B, 0)
        self._write_param(0x0A, 0x70)
        self._write_param(0x0C, 0x20 | 0x04)
        self._write_param(0x0E, 0x00)
        self._write_param(0x1E, 0)
        self._write_param(0x1D, 0x70)
        self._write_param(0x1F, 0x20)
        self._write_param(0x11, 0)
        self._write_param(0x10, 0x70)
        self._write_param(0x12, 0x20)
        self._write8(0x08, 0xFF)
        self._write8(0x18, 0x0F)

    def _read8(self, register):
        """read one byte in hardware sensor"""
        
        self._i2c.write(self._addr, bytearray([register]))
        result = self._i2c.read(self._addr, 1)[0]
        return result

    def _read16(self, register, little_endian=True):
        """read two bytes in hardware sensor"""
        
        self._i2c.write(self._addr, bytearray([register]))
        result = self._i2c.read(self._addr, 2)
        if little_endian:
            result = ((result[1] << 8) | (result[0] & 0xFF))
        else:
            result = ((result[0] << 8) | (result[1] & 0xFF))
        return result

    def _write8(self, register, value):
        """Write one byte on hardware register sensor"""

        self._i2c.write(self._addr, bytearray([register, value & 0xFF]))

    def _write_param(self, parameter, value):
        """Write one value and parameters on hardware sensor"""
        
        self._write8(0x17, value)
        self._write8(0x18, parameter | 0xA0)
        return self._read8(0x2E)

    @property
    def uv(self):
        return self.read_uv()
    
    def read_uv(self):
        """Value of Digital UV Index"""

        return self._read16(0x2C) / 100
    
    @property
    def visible(self):
        return self.read_visible()
    
    def read_visible(self):
        """Value of Visible Ligth"""

        return self._read16(0x22)
    
    @property
    def ir(self):
        return self.read_ir()
    
    def read_ir(self):
        """Value of IR Ligth"""
        return self._read16(0x24)
    
    @property
    def ir_prox(self):
        return self.read_prox()

    def read_prox(self):
        """Value of Proximiti of IR emisor"""
        return self._read16(0x26)
    
if __name__=="__main__":
    # Micro:bit simple teste
    from microbit import i2c
    import time

    sensor = SI1145(i2c=i2c)
    for i in range(10):
        uv = sensor.read_uv()
        ir = sensor.read_ir()
        view = sensor.read_visible()
        print(" UV: {}\n IR: {}\n Visible: {}\n IR prox: {}".format(sensor.uv, sensor.ir, sensor.visible, sensor.ir_prox))
        time.sleep(1)
# # Add your Python code here. E.g.
# from microbit import *
# from stepper_uln2003 import Driver, Command, Stepper, FULL_ROTATION, HALF_STEP

# s1 = Stepper(HALF_STEP, pin13, pin14, pin15, pin16, delay=5)
# # s2 = Stepper(HALF_STEP, microbit.pin6, microbit.pin5, microbit.pin4, microbit.pin3, delay=5)
# #s1.step(FULL_ROTATION)
# #s2.step(FULL_ROTATION)

# print("starting")

# runner = Driver()
# print("runner ok")
# #runner.run([Command(s1, FULL_ROTATION, 1), Command(s1, FULL_ROTATION/2, -1)])
# print("Command end")