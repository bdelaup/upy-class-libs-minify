
"""I2C utils"""

from microbit import i2c

SENSOR_LIST = {
    0x19 : "micro:bit accelerometer", 
    0x1e : "micro:bit Magnetometer", 
    0x3e : "Grove LCD-data", 
    0x62 : "Grove LCD-RGB", 
    0x50 : "Grove Pulsation",
    0x60 : "UV ADF SI1145",
    0x68 : "Gyro grove MPU9250",
    0x73 : "Oxygen DFR SEN0322",
    0x77 : "Pressure ADF BMP390"
    }

def scan_i2c_sensors():
    
    addresses = i2c.scan()
    ret_adresses = []
    for address in i2c.scan():
        if address in SENSOR_LIST:
            print(address, " : ", SENSOR_LIST[address])
            ret_adresses.append(address)
        else :
            print(address, " : unknown")
            
    return addresses

if __name__ == "__main__":
    i2c.init()
    print(scan_i2c_sensors())